import React, { Component } from 'react';
import { Picker, StyleSheet} from "react-native";

class PickerPage extends Component {

    constructor(){
        super();
        this.state={
            language:''
        }
    }

    render() {
        return (
            <Picker
            selectedValue={this.state.language}
            style={{ height: 50, width: 100 }}
            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
            <Picker.Item label="Back End Developer" value="prog" />
            <Picker.Item label="Project Manager" value="man" />
            <Picker.Item label="Front End Developer" value="frony" />
            <Picker.Item label="Business Admin" value="bus" />
            <Picker.Item label="Software Engineer" value="soft" />
            <Picker.Item label="Quality Assurance" value="qa" />
            </Picker>
        );
    }
}

export default PickerPage;