import React, { Component } from 'react';
import { SectionList, View, Text } from "react-native";

class SectionListPage extends Component {
    render() {
        return (
            <SectionList
            renderItem={({item, index, section}) => <Text key={index}>{item}</Text>}
            renderSectionHeader={({section: {title}}) => (
                <Text style={{fontWeight: 'bold'}}>{title}</Text>
            )}
            sections={[
                {title: 'One Piece', data: ['Gomu Gomu no', 'Monkey D. Luffy']},
                {title: 'Naruto Shippuden', data: ['Rasengan', 'Uzumaki Naruto']},
                {title: 'DragonBall', data: ['Kame Kame Ha Wave', 'Son Goku']},
            ]}
            keyExtractor={(item, index) => item + index}
            />
        );
    }
}

export default SectionListPage;