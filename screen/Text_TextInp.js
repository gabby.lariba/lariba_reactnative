import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet } from "react-native";

class Text_TextInp extends Component {
    constructor(props) {
        super(props);
        this.state = {
          titleText: "Bird's Nest",
          bodyText: 'This is not really a bird nest.'
        };
      }

    render() {
        return (
            <View>
                 <Text style={styles.baseText}>
                    <Text 
                        style={styles.titleText} 
                        onPress={this.onPressTitle}>
                        {this.state.titleText}{'\n'}{'\n'}
                    </Text>
                    <Text numberOfLines={5}>
                    {this.state.bodyText}{'\n'}{'\n'}{'\n'}{'\n'}
                    </Text>
                </Text>
                <TextInput
                     style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                     onChangeText={(text) => this.setState({text})}
                     value={this.state.text}
                     placeholder="Type something here"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    baseText: {
    
    },
    titleText: {
      fontSize: 20,
      fontWeight: 'bold',
    },
  });

export default Text_TextInp;