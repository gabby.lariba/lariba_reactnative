import React, { Component } from 'react';
import { View, StatusBar, Text } from "react-native";

class StatusBarPage extends Component {
    render() {
        return (
            <View>
            <StatusBar
              backgroundColor="blue"
              barStyle="light-content"
            />
            <View>
              <StatusBar hidden={true} />
              <Text>Notice that the status is not visible.</Text>
            </View>
          </View>
        );
    }
}

export default StatusBarPage;