import React, { Component } from 'react';
import { SafeAreaView, View, Text } from "react-native";

class SafeArea extends Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#841584'}}>
            <View style={{flex: 1}}>
                <Text>This is a Safe Area!</Text>
            </View>
            </SafeAreaView>
        );
    }
}

export default SafeArea;