import React, { Component } from 'react';
import { 
    View,
    Text,
    DrawerLayoutAndroid
} from "react-native";

class DrawerLayout extends Component {
    render() {
        var navigationView = (
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>This is a View Drawer</Text>
          </View>
        );
        return (
          <DrawerLayoutAndroid
            drawerWidth={300}
            drawerPosition={DrawerLayoutAndroid.positions.Left}
            renderNavigationView={() => navigationView}
            >
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{margin: 10, fontSize: 15, textAlign: 'right'}}>Swipe from the left to view Drawer</Text>
            </View>
          </DrawerLayoutAndroid>
        );
      }
}

export default DrawerLayout;