import React, { Component } from 'react';
import { Text, View, Switch, StyleSheet } from "react-native";


class SwitchPage extends Component {

    state = {
        switchValue: true
    };

    _handleToggleSwitch = () => this.setState(state =>({
        switchValue: !state.switchValue
    }));

    render() {
        return (
            <View style={styles.main}>
                <Text>    Toggle Switch</Text>
                <Switch 
                    onValueChange={this._handleToggleSwitch}
                    value={this.switchValue}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default SwitchPage;