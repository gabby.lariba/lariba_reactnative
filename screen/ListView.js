
import React, { Component } from 'react';
import { ListView, Text } from "react-native";

class ListViewPage extends Component {
    constructor() {
      super();
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        dataSource: ds.cloneWithRows(['Row 1 - ROWAN', 'Row 2 - ROUTE TWO','Row 3 - ROO TREE']),
      };
    }
  
    render() {
      return (
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <Text>{rowData}</Text>}
        />
      );
    }
  }

export default ListViewPage;


