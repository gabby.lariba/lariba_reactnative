import React, { Component } from 'react';
import { FlatList, View, Text, StyleSheet } from "react-native";

class FlatListPage extends Component {
    render() {
        return (
            <View>
            <FlatList
            data={[
                {key: 'Tahong ni Carla Lyrics'}, 
                {key: 'Oh! tahong makarog karog'}, 
                {key: 'makarog karog ang tahong ni carla'},
                {key: 'Oh! tahong makarog karog'},
                {key: 'makarog karog ang tahong ni carla'}
                
            ]}
            renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
            />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: 16,
        height: 40
    },
})

export default FlatListPage;